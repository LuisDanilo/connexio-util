import axios from 'axios'
import { config } from 'dotenv'

// Get env variables
const envVars = config({ path: '.env' })

if ( envVars.parsed ) {
    const { SCALARS_CRUD_URL, SCALARS_COOKIE } = envVars.parsed
    const foo = async () => {
        const response = await axios({
            method: 'POST',
            url: `${SCALARS_CRUD_URL}`,
            headers: {
                Cookie: `admin_cookie=${SCALARS_COOKIE}`
            },
            data: {
                query: `
                    query companies {
                        companies {
                            id name plan { id } plans { id }
                        }
                    }
                `
            },
            
        })
        if ( response.data && response.status === 200 ) {
            const companies = response.data.data.companies
            const responses = await Promise.all( companies.map( ( company: Record<string,any> ) => {
                return new Promise( ( resolve, _reject ) => {
                    return axios({
                        method: 'POST',
                        url: `${SCALARS_CRUD_URL}`,
                        headers: {
                            Cookie: `admin_cookie=${SCALARS_COOKIE}`
                        },
                        data: {
                            query: `
                                mutation updateCompany {
                                    updateCompany(
                                        where: { id: "${company.id}" }
                                        data: {
                                            plans: {
                                                connect: [
                                                    { id: "${company.plan.id}" }
                                                ]
                                            }
                                        }
                                    ){ id plans { id } plan { id } }
                                }
                            `
                        }
                    })
                    .then( res => {
                        if ( res.data && res.status === 200 ) {
                            resolve( {
                                ...res.data.data.updateCompany,
                                message: `Update success`
                            } )
                        } else {
                            resolve({
                                message: `Update failed for company ${company.name} [${company.id}]`
                            })
                        }
                    })
                    .catch( e => {
                        resolve({
                            ...e.data?.errors,
                            message: `Promise failed for company ${company.name} [${company.id}]`
                        })
                    })
                })
            }))
            responses.forEach( (response: any) => {
                console.log( response )
            })
        }
    }
    foo()
}